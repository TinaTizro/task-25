const{Sequelize, QueryTypes } = require('sequelize');
require('dotenv').config();

const sequelize = new Sequelize({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASS,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

async function connect() {
   try {
       await sequelize.authenticate();
       console.log('connection established...');
       const countries = await sequelize.query('SELECT * FROM country', {
          type: QueryTypes.SELECT 
   });
     const countryNames = countries.map(country => country.Name);

     const cities = await sequelize.query('SELECT * FROM city ', {
        type: QueryTypes.SELECT 
 });
      const cityNames = cities.map(city => city.Name);
    console.log(cityNames);
    
    const languages = await sequelize.query('SELECT * FROM world.countrylanguage',{
        type: QueryTypes.SELECT 
 });
      const countryLanguages = languages.map(country => country.Language);
    console.log(languages);

   }
   catch (e) {
       console.error(e);

   }
}
connect();
